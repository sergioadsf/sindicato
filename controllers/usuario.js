module.exports = function(app){

	var mongoose = require('mongoose');
	var model = mongoose.model('usuario');
	var jwt = require('jsonwebtoken');

	var UsuarioController = {
		salvar: function(req, res) {
			var usuario = req.body; 
			usuario.ativo = true;
			model.create(usuario)
			.then(function(data, status){
				console.log("Salvou - "+data);
				console.log("Status - "+status);
			}, function(error){
				console.log("Error - "+error);
			})	
		},
		editar: function(req, res) {
			model
			.findByIdAndUpdate({_id: req.params.idUsuario}, req.body)
			.then(function(data, status){
				console.log("Editou - "+data);
				console.log("Status - "+status);
			}, function(error){
				console.log("Error - "+error);
			})
		},
		conectar: function(req, res){
			var usuario = req.body; 
			var response = app.models.response;
			model
			.findOne({login: usuario.login, senha: usuario.senha})
			.then(function(data) {
				if(data){
					jwt.sign({login: usuario.login}, app.get('secret'), {
						expiresIn: '1d'
					}, function(err, token){
						if(token){
							res.set('x-access-token', token);
							usuario.token = token;
							response.err = false;
							response.data = usuario;
							res.json(response);
						}
						console.log(err);	
					});
				}else{
					res.status(401).send('Login e senha inválidos');
				}
			});
		},
		listar: function(req, res){
			var pagina = req.query.pagina;
			var total = parseInt(req.query.total);
			model.paginate({}, {page: pagina, limit: total, select: "nome login"})
			.then(function(data){
				res.json(data);
			});
		}
	}
	

	return UsuarioController;
}