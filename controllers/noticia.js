"use strict";

module.exports = function(app){

	const informativoDAO = new app.database.informativoDAO();
	const Controller = {}
	const tipo = "NOTICIA";

	Controller.salvar = function(req, res){
		let informativo = req.body;
		informativo.tipo = tipo;

		informativoDAO.salvar(informativo);
		model.create(informativo, function(data, status){
			res.json(data);
		}, function(error){
			res.json(error);
		})
	}

	Controller.listar = function(req, res){
		let pagina = req.query.pagina;
		let total = parseInt(req.query.total);

		informativoDAO.listar(tipo, pagina, total, "titulo imagem.nome imagem.tipo", function(data){
			res.json(data);
		});
		
	} 

	Controller.consultar = function(req, res){
		informativoDAO.consultar(req.params.id, function (data) {
			console.log("Consultou");
			console.log(data);
			res.json(data);
		});
	}
	
	Controller.upload = function(req, res){
		let id = req.body.data._id;
		let file = req.file;
		let imagem = {tipo: file.mimetype, nome: file.filename, caminho: file.path};

		informativoDAO.salvarImagem(id, imagem, function(data){
			res.json(id);
		});
		console.log(req.body, req.file);
	}

	return Controller;
}