"use strict";

module.exports =  function(app){

	const informativoDAO = new app.database.informativoDAO();
	const Controller = {};
	const tipo = "EVENTO";

	Controller.salvar = function(req, res){
		let informativo = req.body;
		informativo.tipo = tipo;
		informativoDAO.salvar(informativo, function (data, status){
			res.json(data);
		}, function(error){
			res.json(error);
		});
	}
	
	Controller.listar = function(req, res){
		let pagina = req.query.pagina;
		let total = parseInt(req.query.total);
		
		informativoDAO.listar(tipo, pagina, total, "titulo ", function(data){
			res.json(data);
		});
	}
	
	Controller.consultar = function(req, res){
		informativoDAO.consultar(req.params.id, function (data) {
			console.log(data);
			res.json(data);
		});
	}

	

	return Controller;
}