angular.module('talk')
.config(function ($httpProvider) {
	$httpProvider.interceptors.push('httpInterceptor');
	$httpProvider.interceptors.push('testeService');
});