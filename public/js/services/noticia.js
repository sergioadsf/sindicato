angular.module('talk')
.factory('NoticiaService', function($http, InformativoService){

	var _salvarOuEditar = function(noticia){
		return InformativoService.salvarOuEditar('noticia', noticia)
	}

	var _listar = function(pagina, total){
		return InformativoService.listar('noticia', pagina, total);
	}

	var _consultar = function(id){
		return InformativoService.consultar('noticia', id);
	}

	return{
		listar: _listar,
		consultar: _consultar,
		salvarOuEditar: _salvarOuEditar
	}
})