angular.module('Convenio')
.factory('ConvenioService', function($http, InformativoService){
	
	var _salvarOuEditar = function(convenio){
		return InformativoService.salvarOuEditar('convenio', convenio)
	}

	var _listar = function(pagina, total){
		return InformativoService.listar('convenio', pagina, total);
	}

	var _consultar = function(id){
		return InformativoService.consultar('convenio', id);
	}

	return{
		listar: _listar,
		consultar: _consultar,
		salvarOuEditar: _salvarOuEditar
	}
});