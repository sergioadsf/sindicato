angular.module('Evento')
.factory('EventoService', function($http, InformativoService){

	var _salvarOuEditar = function(evento){
		return InformativoService.salvarOuEditar('evento', evento)
	}

	var _listar = function(pagina, total){
		return InformativoService.listar('evento', pagina, total);
	}

	var _consultar = function(id){
		return InformativoService.consultar('evento', id);
	}

	return{
		listar: _listar,
		consultar: _consultar,
		salvarOuEditar: _salvarOuEditar
	}
});