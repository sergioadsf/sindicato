angular.module('talk')
.factory('UsuarioService', function($http){

	var _salvarOuEditar = function(usuario){
		if(usuario._id){
			return _editar(usuario);
		}else{
			return _salvar(usuario);
		}
	}

	var _salvar = function(usuario){
		return $http.post('/v1/usuario', usuario);
	}

	var _editar = function(usuario){
		return $http.put(`/v1/usuario/${usuario._id}`, usuario);
	}

	var _conectar = function(usuario) {
		return $http.post('/v1/usuario/conectar', usuario);
	}

	var _listar = function(pagina, total){
		pagina = pagina || 1;
		total = total || 2;
		return $http.get(`/v1/usuario?pagina=${pagina}&total=${total}`);
	}

	return{
		conectar: _conectar,
		listar: _listar,
		salvarOuEditar: _salvarOuEditar
	}
})