angular.module('testeM', []);
angular.module('testeM')
.factory('testeService', function($rootScope, $q, $timeout){
	return {
		request: function(config){
			$rootScope.isError = false;
			console.log("request "+config);
			return config;
		},
		response: function(response){
			console.log("response "+response);
			return response;
		},
		responseError: function(error){
			console.log("responseError "+error);
			$rootScope.isError = true;
			$rootScope.error = error.data;
			$timeout(function(){
				$rootScope.isError = false;
				$rootScope.error = "";
			}, 2000)
			return $q.reject(error);
		}
	}
})
