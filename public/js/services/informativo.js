angular.module('Informativo', []);
angular.module('Informativo')
.factory('InformativoService', function($http){

	var _salvarOuEditar = function(path, informativo){
		if(informativo._id){
			return _editar(path, informativo);
		}else{
			return _salvar(path, informativo);
		}
	}

	var _salvar = function(path, informativo){
		return $http.post(`/v1/${path}`, informativo);
	}

	var _consultar = function(path, id){
		return $http.get(`/v1/${path}/${id}`);
	}

	var _editar = function(path, informativo){
		return $http.put(`/v1/${path}/${noticia._id}`, informativo);
	}

	var _listar = function(path, pagina, total){
		pagina = pagina || 1;
		total = total || 2;
		return $http.get(`/v1/${path}?pagina=${pagina}&total=${total}`);
	}

	return{
		listar: _listar,
		consultar: _consultar,
		salvarOuEditar: _salvarOuEditar
	}
});
