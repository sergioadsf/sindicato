angular.module('talk')
.factory('UploadService', function(Upload){

	var _upload = function(url, file, obj){
		var data = {data: obj, image: file};

		return Upload.upload({
			url: url,
			headers : {
				'Content-Type': file.type
			},
			data: data
		})
	}

	return{
		upload: _upload
	}
})