"use strict";

angular.module('Noticia')
.controller('NoticiaDetailController', NoticiaDetailController);

function NoticiaDetailController($scope, $stateParams, NoticiaService, UploadService) {
    let vm = this;

    $scope.$on('formulario_limpar', function(event){
    	_limpar();
    })

    $scope.$on('formulario_salvar', function(event){
    	_salvar();
    })

    vm.submit = function() {
        if (vm.file) {
            vm.upload(vm.file);
        }
    };
    
    vm.upload = function (file) {

        UploadService.upload('v1/noticia/upload', vm.file, vm.noticia)
        .then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
            console.log('Error status: ' + resp.status);
            console.log(resp);
        }, function (evt) {
            let progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };

    let _limpar = function(){
    	vm.noticia = {};
    }

    let _consultar = function(id){
        NoticiaService.consultar(id)
        .success(function(data){
            vm.noticia = data;
            vm.noticia.imagem.caminho = vm.noticia.imagem.caminho.replace("public\\", "");
        })
        .error(function(data, status){
            console.log(data);
            console.log(status);
        })
    }

    let _salvar = function(){
    	NoticiaService.salvarOuEditar(vm.noticia)
    	.success(function(data){
            vm.noticia = data;
            vm.submit();
        })
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	})
    }

    vm.init = function(){
        vm.noticia = {};
        let id = $stateParams.id;
        if(id != 'new'){
            _consultar(id);
        }
    }

};