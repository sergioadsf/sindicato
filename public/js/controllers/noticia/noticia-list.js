"use strict";

angular.module('Noticia',[])
.controller('NoticiaListController', NoticiaListController);

function NoticiaListController(NoticiaService) {

    let vm = this;
    let _listar = function(pagina){
    	NoticiaService.listar(pagina, vm.itensPagina)
    	.success(function(data){
    		vm.lista = data.docs;
    		vm.totalNoticias = parseInt(data.total);
    		vm.paginaAtual = parseInt(data.page);
    		vm.totalPaginas = parseInt(data.pages);
    	})
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	});
    }

    vm.initLista = function(){
    	vm.itensPagina = 5;
    	_listar(1);
    }
};