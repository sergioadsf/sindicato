angular.module('talk')
.controller('NoticiaController', function($scope, NoticiaService, UploadService) {

	$scope.noticia = {};

    $scope.$on('formulario_limpar', function(event){
    	_limpar();
    })

    $scope.$on('formulario_salvar', function(event){
    	_salvar();
    })

    var _limpar = function(){
    	$scope.noticia = {};
    }

    $scope.carregarEditar = function(noticia){
    	$scope.noticia = noticia;
    }

    var _salvar = function(){
    	NoticiaService.salvarOuEditar($scope.noticia)
    	.success(function(data){
    		_listar(pagina);
    		console.log(data);
    	})
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	})
    }

    _listar = function(pagina){
    	NoticiaService.listar(pagina, $scope.itensPagina)
    	.success(function(data){
    		$scope.lista = data.docs;
    		$scope.totalNoticias = parseInt(data.total);
    		$scope.paginaAtual = parseInt(data.page);
    		$scope.totalPaginas = parseInt(data.pages);
    		console.log(data);
    	})
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	});
    }

    var init = function(){
    	$scope.itensPagina = 5;
    	$scope.isSenhaValida = true;
    	_listar(1);
    }

    init();
});