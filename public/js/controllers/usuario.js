angular.module('Usuario', [])
.controller('UsuarioController', UsuarioController);

function UsuarioController($scope, UsuarioService){
	var vm = this;

	vm.usuario = {};

	vm.submit = function() {
		if (vm.form.file.$valid && vm.file) {
			vm.upload(vm.file);
		}
	};

	vm.upload = function (file) {

		UploadService.upload('upload/teste', vm.file, vm.usuario)
		.then(function (resp) {
			console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		});

	};

	$scope.$on('formulario_limpar', function(event){
		_limpar();
	})

	$scope.$on('formulario_salvar', function(event){
		_salvar();
	})

	var _limpar = function(){
		vm.usuario = {};
	}

	vm.validarSenha = function(){
		vm.isSenhaValida = vm.usuario.repetirSenha == vm.usuario.senha;
	}

	vm.carregarEditar = function(usuario){
		vm.usuario = usuario;
	}

	var _salvar = function(){
		vm.usuario.perfil = "ADMINISTRADOR";
		UsuarioService.salvarOuEditar(vm.usuario)
		.success(function(data){
			_listar(pagina);
			console.log(data);
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		})
	}

	_listar = function(pagina){
		UsuarioService.listar(pagina, vm.itensPagina)
		.success(function(data){
			vm.lista = data.docs;
			vm.totalUsuarios = parseInt(data.total);
			vm.paginaAtual = parseInt(data.page);
			vm.totalPaginas = parseInt(data.pages);
			console.log(data);
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		});
	}

	var init = function(){
		vm.itensPagina = 5;
		vm.isSenhaValida = true;
		_listar(1);
	}

	init();

};