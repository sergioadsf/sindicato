angular.module('Login', [])
.controller('LoginController', LoginController)

function LoginController($rootScope, $location, UsuarioService, $http){
	var vm = this;
	vm.isLogado = false;
	vm.usuario = {login: 'sergio', senha: '12345'};

	vm.conectar = function() {
		UsuarioService.conectar(vm.usuario)
		.success(function(data){
			if(data.error){
				vm.mensagem = data.message;
				vm.usuario = {};
				return;
			}
			$rootScope.isLogado = true;
			$location.path('/principal');
		})
		.error(function(data, status) {
			console.log(data);
			console.log(status);
		});
	}

	vm.novoUsuario = novoUsuario;
	function novoUsuario(){
		$location.path('/novousuario');
	}

	vm.init = function(){
		let pagina = 1;
		let total = 10;
		$http.get(`/v1/usuario?pagina=${pagina}&total=${total}`)
		.success(function(data){
			console.log(new Date());
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		});
	}

}
