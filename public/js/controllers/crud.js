angular.module('talk')
.controller('CrudTesteController', function($scope, UsuarioService, UploadService) {

	$scope.usuario = {};

	/*$scope.$on("datatable_listar", function(event, pagina){
		_listar(pagina);
	});
	*/

	$scope.submit = function() {
		if ($scope.form.file.$valid && $scope.file) {
			$scope.upload($scope.file);
		}
	};

	$scope.upload = function (file) {
		
		UploadService.upload('upload/teste', $scope.file, $scope.usuario)
		.then(function (resp) {
			console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    		//console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
    	});

	};
    // fo

    $scope.$on('formulario_limpar', function(event){
    	_limpar();
    })

    $scope.$on('formulario_salvar', function(event){
    	_salvar();
    })

    var _limpar = function(){
    	$scope.usuario = {};
    }

    $scope.validarSenha = function(){
    	$scope.isSenhaValida = $scope.usuario.repetirSenha == $scope.usuario.senha;
    }

    $scope.carregarEditar = function(usuario){
    	$scope.usuario = usuario;
    }

    var _salvar = function(){
    	UsuarioService.salvarOuEditar($scope.usuario)
    	.success(function(data){
    		_listar(pagina);
    		console.log(data);
    	})
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	})
    }

    _listar = function(pagina){
    	UsuarioService.listar(pagina, $scope.itensPagina)
    	.success(function(data){
    		$scope.lista = data.docs;
    		$scope.totalUsuarios = parseInt(data.total);
    		$scope.paginaAtual = parseInt(data.page);
    		$scope.totalPaginas = parseInt(data.pages);
    		console.log(data);
    	})
    	.error(function(data, status){
    		console.log(data);
    		console.log(status);
    	});
    }

    var init = function(){
    	$scope.itensPagina = 5;
    	$scope.isSenhaValida = true;
    	_listar(1);
    }

    init();
});