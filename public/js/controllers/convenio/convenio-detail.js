angular.module('Convenio')
.controller('ConvenioDetailController', ConvenioDetailController);

function ConvenioDetailController(ConvenioService, $scope, $stateParams){

	let vm = this;

	$scope.$on('formulario_limpar', function(event){
		_limpar();
	})

	$scope.$on('formulario_salvar', function(event){
		_salvar();
	})

	let _limpar = function(){
		vm.convenio = {};
	}

	let _consultar = function(id){
		EventoService.consultar(id)
		.success(function(data){
			vm.convenio = data;
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		})
	}

	let _salvar = function(){
		EventoService.salvarOuEditar(vm.convenio)
		.success(function(data){
			vm.convenio = data;
			vm.submit();
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		})
	}

	vm.init = function(){
		vm.convenio = {};
		let id = $stateParams.id;
		if(id != 'new'){
			_consultar(id);
		}
	}

}