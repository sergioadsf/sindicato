"use strict";

angular.module('Convenio', [])
.controller('ConvenioListController', ConvenioListController);

function ConvenioListController(ConvenioService){
	
	let vm = this;
	
	let _listar = function(pagina){
		ConvenioService.listar(pagina, vm.itensPagina)
		.success(function(data){
			vm.lista = data.docs;
			vm.totalEventos = parseInt(data.total);
			vm.paginaAtual = parseInt(data.page);
			vm.totalPaginas = parseInt(data.pages);
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		});
	}

	vm.initLista = function(){
		vm.itensPagina = 5;
		_listar(1);
	}
};