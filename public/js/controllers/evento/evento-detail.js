"use strict";

angular.module('Evento')
.controller('EventoDetailController', EventoDetailController);

function EventoDetailController(EventoService, $scope, $stateParams){
	
	let vm = this;

	$scope.$on('formulario_limpar', function(event){
		_limpar();
	})

	$scope.$on('formulario_salvar', function(event){
		_salvar();
	})

	let _limpar = function(){
		vm.evento = {};
	}

	let _consultar = function(id){
		EventoService.consultar(id)
		.success(function(data){
			vm.evento = data;
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		})
	}

	let _salvar = function(){
		EventoService.salvarOuEditar(vm.evento)
		.success(function(data){
			vm.evento = data;
			vm.submit();
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		})
	}

	vm.init = function(){
		vm.evento = {};
		let id = $stateParams.id;
		if(id != 'new'){
			_consultar(id);
		}
	}
};