angular.module('talk')
.directive('datatable', function(){
	return {
		restrict: 'E',
		transclude: true,
		templateUrl: '/partials/datatable.html',
		scope: {
			titulo: "@",
			link: "@",
			scope: '='
		},
		link: function($scope){

			$scope.montarPaginas = function(paginas){
				$scope.scope;
				return new Array(paginas);
			}

			$scope.carregarPagina = function(pagina){
				if(pagina > 0 && pagina <= $scope.scope.totalPaginas){
					//$scope.$emit("datatable_listar", pagina);
					_listar(pagina);
				}
			}
		}
	};
});