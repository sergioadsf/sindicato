angular.module('talk')
.directive('formCrud', function(){
	return {
		restrict: 'E',
		templateUrl: '/partials/formulario.html',
		//template: '<h1>Sergio {{titulo}}</h1>',
		transclude: true,
		scope: {
			titulo: "@",
			mensagem: "="
		},
		link: function($scope){
			$scope.limpar = function(){
				$scope.$emit('formulario_limpar');
			}

			$scope.salvar = function(){
				$scope.$emit('formulario_salvar');
			}

			$scope.voltar = function() {
				history.back();
			}
		}
	};
});