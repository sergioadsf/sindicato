angular.module('talk',[
	'ui.router',
	//'ui.bootstrap',
	'ngFileUpload',
	'Login',
	'Usuario', 
	'Noticia',
	'Evento',
	'Convenio',
	'Informativo',
	'testeM'
	])
.run(function($rootScope) {
	document.addEventListener("keyup", function(e) {
		if (e.keyCode === 27)
			$rootScope.$broadcast("escapePressed", e.target);
	});

	document.addEventListener("click", function(e) {
		$rootScope.$broadcast("documentClicked", e.target);
	});
})
.config(function($stateProvider, $urlRouterProvider, $locationProvider){

	$urlRouterProvider.otherwise('/');

	//$httpProvider.interceptors.push('httpInterceptor');

	$stateProvider
	.state('home', {
		url: '/',
		templateUrl: 'views/login.html',
		controller: 'LoginController',
		controllerAs: 'Login'
	})

	$stateProvider
	.state('crud', {
		url: '/crud',
		templateUrl: 'views/usuario/crud.html',
		controller: 'CrudTesteController'
	})
	.state('novousuario', {
		url: '/novousuario',
		templateUrl: 'views/usuario/novo-usuario.html',
		controller: 'UsuarioController',
		controllerAs: 'Usuario'
	})

	.state('principal', {
		url: '/principal',
		templateUrl: 'views/principal.html',
		controller: 'PrincipalController',
		controllerAs: 'Principal'
	});

	$stateProvider
	.state('noticias', {
		abstract: true,
		url: '/noticias',
		templateUrl: 'views/noticia/noticia.html',
	})
	.state('noticias.list', {
		url: '',
		templateUrl: 'views/noticia/noticia-list.html',
		controller: 'NoticiaListController',
		controllerAs: 'NoticiaList'
	})
	.state('noticias.carregar', {
		url: '/:id',
		templateUrl: 'views/noticia/noticia-detail.html',
		controller: 'NoticiaDetailController',
		controllerAs: 'NoticiaDetail'
	});

	$stateProvider
	.state('eventos', {
		abstract: true,
		url: '/eventos',
		templateUrl: 'views/evento/evento.html',
	})
	.state('eventos.list', {
		url: '',
		templateUrl: 'views/evento/evento-list.html',
		controller: 'EventoListController',
		controllerAs: 'EventoList'
	})
	.state('eventos.carregar', {
		url: '/:id',
		templateUrl: 'views/evento/evento-detail.html',
		controller: 'EventoDetailController',
		controllerAs: 'EventoDetail'
	});

	$stateProvider
	.state('convenios', {
		abstract: true,
		url: '/convenios',
		templateUrl: 'views/convenio/convenio.html'
	})
	.state('convenios.list', {
		url: '',
		templateUrl: 'views/convenio/convenio-list.html',
		controller: 'ConvenioListController',
		controllerAs: 'ConvenioList'
	})
	.state('convenios.carregar', {
		url: '/:id',
		templateUrl: 'views/convenio/convenio-detail.html',
		controller: 'ConvenioDetailController',
		controllerAs: 'ConvenioDetail'
	})

	$locationProvider.html5Mode(true);
});