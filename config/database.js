var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var mpromise = require('mpromise');
var assert = require('assert'); 

var url = 'mongodb://localhost:27017/sindicato';

mongoose.connect(url);

mongoose.connection.on('connected', function(){
	console.log("Connected");
});

mongoose.connection.on('error', function(error){
	console.log("Error => "+error);
});

mongoose.connection.on('disconnected', function(){
	console.log("Disconnected");
});

process.on('SIGINT', function(){
	mongoose.connection.close(function(){

		console.log('Fechou aplicação');
		process.exit(0);
	});
});
