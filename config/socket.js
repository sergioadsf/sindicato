module.exports = function(http){
	var io = require('socket.io')(http);

	io.on('connection', function(socket){
		socket.on('chat_message', function(msg){
			io.emit('chat_message', msg);
		});
	});
}