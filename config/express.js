var express = require('express');
var app = express();
var load = require('express-load');
var bodyParser = require('body-parser');
var fs = require('fs');

//var multipart = require('connect-multiparty');
//var multipartMiddleware = multipart();

app.set('secret', 'testetoken');
app.use(express.static('./public'));
app.use("/node_modules", express.static('node_modules'));
app.use(bodyParser.json());

app.get('*', function(req, res, next) {
	//res.sendfile('./public/index.html'); 
	next();
});

load('models')
.then('database')
.then('controllers')
.then('routes')
.into(app);

// Teste

module.exports = app;
