var app = require('./config/express');
var db = require('./config/database');
var http = require('http').Server(app);
var io = require('./config/socket')(http);

http.listen(3000, function(){
	console.log('listening on *:3000');
});