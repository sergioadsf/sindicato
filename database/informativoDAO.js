"use strict";

let mongoose = require('mongoose');
let model = mongoose.model('baseinformativo');

function InformativoDAO(){

}

InformativoDAO.prototype.salvar = function(informativo, callback){
	model.create(informativo).then(callback);
}

InformativoDAO.prototype.listar = function(tipo, pagina, total, campoSelect, callback){
	model.paginate({tipo: tipo}, {page: pagina, limit: total, select: campoSelect}).then(callback);
}

InformativoDAO.prototype.consultar = function(id, callback){
	model.findById(id).then(callback);
}

InformativoDAO.prototype.salvarImagem = function(id, imagem, callback){
	model.update({_id: id}, {$set: {imagem: imagem}}).then(callback);
}

module.exports = function(){
	return InformativoDAO;
}
