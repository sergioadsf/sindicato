module.exports = function(app) {

	var usuario = app.controllers.usuario;
	
	app.route('/v1/usuario')
	.post(usuario.salvar)
	.get(usuario.listar);

	app.route('/v1/usuario/:idUsuario')
	.put(usuario.editar);

	app.post('/v1/usuario/conectar', usuario.conectar); 
}