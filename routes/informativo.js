"use strict";

var multer  = require('multer')
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, './public/uploads/noticias')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname)
	}
});

var upload = multer({ storage: storage });
module.exports = function(app){
	
	let noticia = app.controllers.noticia;

	app.route('/v1/noticia')
	.post(noticia.salvar)
	.get(noticia.listar);

	app.route('/v1/noticia/:id')
	.get(noticia.consultar);
	//.put(noticia.atualizar);


	app.post('/v1/noticia/upload', upload.single('image'), noticia.upload);

	let evento = app.controllers.evento;

	app.route('/v1/evento')
	.post(evento.salvar)
	.get(evento.listar);

	app.route('/v1/evento/:id')
	.get(evento.consultar);

	let convenio = app.controllers.convenio;

	app.route('/v1/convenio')
	.post(convenio.salvar)
	.get(convenio.listar);

	app.route('/v1/convenio/:id')
	.get(convenio.consultar);
}