var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var imagem = require('./imagem');

var Schema = mongoose.Schema;

var baseSchema = new Schema({
	titulo: {
		type: String,
		required: true
	},
	texto: {
		type: String,
		required: true
	},
	tipo: {
		type: String,
		required: true	
	},
	area: {
		type: String,
	},
	email: {
		type: String,
	},
	imagem: imagem
});

/*baseSchema.static('findById', function(id, callback){
	return this.find({_id: id})
});*/

baseSchema.static('findByTitulo', function(titulo, callback){
	return this.find({titulo: titulo})
});

baseSchema.plugin(mongoosePaginate);

mongoose.model('baseinformativo', baseSchema);

module.exports = baseSchema;