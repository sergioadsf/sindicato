var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

var categoriaSchema = new Schema({
	nome: {
		type: String,
		required: true
	}

});

categoriaSchema.static('findByName', function(nome, callback){
	return this.find({nome: nome})
});

categoriaSchema.plugin(mongoosePaginate);

mongoose.model('categoria', categoriaSchema);

module.exports = categoriaSchema;