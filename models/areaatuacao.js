var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

var areaAtuacaoSchema = new Schema({
	nome: {
		type: String,
		required: true
	}
});

areaAtuacaoSchema.static('findByName', function(nome, callback){
	return this.find({nome: nome})
});

areaAtuacaoSchema.plugin(mongoosePaginate);

mongoose.model('areaatuacao', areaAtuacaoSchema);

module.exports = areaAtuacaoSchema;