module.exports = {
	nome: {
		type: String,
		required: false
	},
	tipo: {
		type: String,
		required: false
	},
	caminho: {
		type: String,
		required: false
	}
}