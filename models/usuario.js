var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
	nome: {
		type: String,
		required: true
	},
	cpf: {
		type: String,
		required: false
	},
	login: {
		type: String,
		required: true
	},
	senha: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: false
	},
	telefone: {
		type: String,
		required: false
	},
	dataNascimento: {
		type: Date,
		required: false
	},
	dataCadastro: {
		type: Date,
		required: false
	},
	dataUltimoAcesso: {
		type: Date,
		required: false
	},
	ativo: {
		type: Boolean,
		required: true
	},
	perfil: {
		type: String,
		required: true
	},
	gcm: {
		type: String,
		required: false
	},
	tipoPush: {
		type: String,
		required: false
	},
	senha2: {
		type: String,
		required: false
	},
	codigo_recuperar_senha: {
		type: String,
		required: false
	},
	areaAtuacao: {
		type: String,
		required: false
	},
	categoria: {
		type: String,
		required: false
	}
});

usuarioSchema.static('findByName', function(nome, callback){
	return this.find({nome: nome})
});

usuarioSchema.plugin(mongoosePaginate);

mongoose.model('usuario', usuarioSchema);